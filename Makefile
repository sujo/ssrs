default: build

DESTDIR?="/usr/local"
PREFIX?="/"

build:
	c99 ssrs.c -o ssrs
clean:
	rm ssrs
install: build
	mkdir -p ${DESTDIR}${PREFIX}bin
	cp -f ssrs ${DESTDIR}${PREFIX}bin
	chmod 755 ${DESTDIR}${PREFIX}bin/ssrs
	mkdir -p ${DESTDIR}${PREFIX}man/man1
	cp -f ssrs.1 ${DESTDIR}${PREFIX}man/man1/
	chmod 655 ${DESTDIR}${PREFIX}man/man1/ssrs.1
