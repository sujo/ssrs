#include "ssrs.h"

int
main(int argc, char** argv) {
	if (argc < 4)
		die("too few arguments\n");

	const int index = atoi(argv[2]);
	if (index <= 0)
		die("invalid index\n");

	switch (argv[1][1]) {
		case 'g': {
			char cards[argc - 3][128];
			for (int i = 3; i < argc - 3; i++)
				strcpy(cards[i - 3], argv[i]);
			get(argc - 3, cards, index);
			break;
		}
		case 's':
			set(argv[3], index);
			break;
		default:
			die("incorrect argument order\n");
	}

	return 0;
}
