#ifndef __ssrs_h
#include <stdio.h>
#include <fcntl.h>
#include <sys/stat.h> // system bits
#include <unistd.h>
#include <stdlib.h>
#include <sys/time.h>
#include <libgen.h>
#include <string.h>
#include <stdarg.h>

static const int strmax	= 1000; // max len of everything

void
die(const char* fmt, ...) {
	va_list va;

	va_start(va, fmt);
	vfprintf(stderr, fmt, va);
	va_end(va);

	exit(1);
}

static void
cache_filepath(char* out, int len, char* path) {
	char* home = getenv("HOME");
	char* dir = getenv("SSRS_CACHE");
	if (strlen(dir) == 0)
		dir = ".cache/ssrs";

	char* bname = basename(path);
	snprintf(out, len, "%s/%s/%s", home, dir, bname);
}

void
set(char* path, int index) {
	// get cache path
	char cpath[strmax];
	cache_filepath(cpath, sizeof(cpath), path);

	// fill in access time
	struct timeval v;
	gettimeofday(&v, NULL);

	// updated state data
	int state[2] = {index, v.tv_sec};

	int fd = open(cpath, (O_WRONLY | O_TRUNC | O_CREAT), (S_IRUSR | S_IWUSR));
	if (fd == -1)
		die("could not open or create file: %s\n", path);

	// write state
	char h[strmax];
	int w = snprintf(h, sizeof(h), "%d\n%d\n", state[0], state[1]);
	if (w < 0)
		die("could not format state string\n");

	if (write(fd, h, w) <= 0)
		die("could not write to file: %s\n", path);

	close(fd);

	printf("%s\n", cpath);
}

void
get(int cardc, char** cards, int index) {
	int lowest = 2147483647; // 2^31 - 1 ;)
	char lowest_path[strmax];

	// loop over all cards
	for (int i = 0; i < cardc; i++) {
		int state[2] = {-1, -1};
		char cpath[strmax];

		cache_filepath(cpath, sizeof(cpath), cards[i]);

		int fd = open(cpath, O_RDONLY);
		if (fd == -1)
			die("could not open cache file: %s, for card: %s\n", cpath, cards[i]);

		// collect status information
		for (int j = 0; j < 2; j++) {
			char k[strmax], c;
			int l = 0;
			for (int l = 0; l < strmax && read(fd, &c, 1) && c != '\n'; l++)
				k[l] = c;
			k[l + 1] = '\0';
			state[j] = atoi(k);
		}
		close(fd);

		if (state[0] == index && state[1] < lowest) {
			lowest = state[1];
			strcpy(lowest_path, cpath);
		}
	}
	if (strlen(lowest_path) == 0)
		die("no cards on index %d", index);
	printf("%s\n", lowest_path);
}

#define __ssrs_h
#endif
